﻿using System.ComponentModel;
using System.Diagnostics.Contracts;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Timers;
using MatrixClient.Schema;
using MatrixClient.Schema.Enums;
using MatrixClient.Schema.Events;
using MatrixClient.Schema.Id;
using MatrixClient.Schema.Login;
using MatrixClient.Schema.Responses;
using MatrixClient.Serialization;
using RestSharp;

namespace MatrixClient;

public class Client {
    private readonly RestClient client;
    private readonly System.Timers.Timer PollingTimer;
    private CancellationTokenSource? pollCancellation;
    public static readonly TimeSpan PollInterval = TimeSpan.FromMilliseconds(1000);
    public Client(string host) {
        var uri = new UriBuilder(Uri.UriSchemeHttps, host).Uri;
        client = new(options: new RestClientOptions() {
            BaseUrl = uri,
            ConfigureMessageHandler = i => new ErrorHandler(i)
        });
        client.UseNewtonsoftJsonSerializer(new() {
            DefaultValueHandling = DefaultValueHandling.Ignore
        });
        PollingTimer = new(TimeSpan.FromSeconds(1)) {
            AutoReset = false,
            Enabled = false
        };
        PollingTimer.Elapsed += Timer_Elapsed;
        EventHappened += Client_EventHappened;
    }

    private void Timer_Elapsed(object? sender, ElapsedEventArgs e) {
        pollCancellation?.Cancel();
        pollCancellation = new();
        _ = Task.Run(() => PollOnce(pollCancellation.Token));
    }

    private async Task PollOnce(CancellationToken ct) {
        var sync = await SyncAsync(syncBatch, ct);
        if (sync.Presence is not null and var presences)
            foreach (var presence in presences)
                EventHappened?.Invoke(this, presence);
        if (sync.AccountData is not null and var accountData)
            foreach (var data in accountData) {
                if (data is UnknownEvent unknown) Console.WriteLine($"Warning: unknown event of type {unknown.Type} was recorded!");
                else
                    EventHappened?.Invoke(this, data);
            }
        foreach (var (room, joined) in sync.Rooms?.Join ?? Enumerable.Empty<KeyValuePair<string, JoinedRoom>>())
            foreach (var evt in joined.Timeline?.Events ?? Enumerable.Empty<Event>())
                EventHappened?.Invoke(this, evt);
        syncBatch = sync.NextBatch;
        pollCancellation = null;
        PollingTimer.Start();
    }

    public void StartListening(string? syncBatch = null) {
        if (PollingTimer.Enabled) throw new InvalidOperationException("Polling service already running!");
        this.syncBatch = syncBatch;
        PollingTimer.Start();
    }

    public string? StopListening() {
        PollingTimer.Stop();
        pollCancellation?.Cancel();
        while (pollCancellation is not null) Thread.Sleep(20);
        return syncBatch;
    }

    public async Task<SyncResponse> SyncAsync(string? syncBatch = null, CancellationToken ct = default) {
        var request = new RestRequest("/_matrix/client/v3/sync", Method.Get);
        if (syncBatch is not null) request.AddQueryParameter("since", syncBatch);
        var response = await client.GetAsync<SyncResponse>(request, ct);
        ArgumentNullException.ThrowIfNull(response);
        return response;
    }

    public string DeviceId { get; set; } = "C# Matrix Client";
    public string UserId { get => userId ?? throw new InvalidOperationException("User is not logged in!"); private set => userId = value; }
    public event EventHandler<Event>? EventHappened;

    private string? userId;
    private string? accessToken;
    private string? syncBatch;

    public Task LoginAsync(string username, string password, CancellationToken ct = default)
        => LoginAsync(new PasswordLogin() {
            Identifier = new User() {
                UserId = username
            },
            DeviceId = DeviceId,
            Password = password
        }, ct);

    public async Task LoginAsync(Login login, CancellationToken ct = default) {
        var response = await client.PostJsonAsync<Login, LoginResponse>("/_matrix/client/v3/login", login, ct);
        ArgumentNullException.ThrowIfNull(response);
        await LoginSuccessful(response);
    }

    private Task LoginSuccessful(LoginResponse response) {
        DeviceId = response.DeviceId;
        UserId = response.UserId;
        accessToken = response.AccessToken;
        client.AddDefaultHeader("Authorization", $"Bearer {accessToken}");
        return Task.CompletedTask;
    }

    public Task LogoutAsync(CancellationToken ct = default)
        => client.PostJsonAsync<object>("/_matrix/client/v3/logout", new(), ct);

    public IAsyncEnumerable<Room> GetPublicRoomsAsync(CancellationToken ct = default)
        => PaginationAsync<Room, RoomPaginationResponse>(
            "/_matrix/client/v3/publicRooms",
            i => i.Items, (i, j) => i.PrepareNext(j),
            ct: ct);

    private IAsyncEnumerable<TItem> PaginationAsync<TItem, TResponse>(string path, Func<TResponse, IEnumerable<TItem>> enumerateResponse, Func<TResponse, RestRequest, bool> prepareNext, int? limit = null, CancellationToken ct = default) {
        var request = new RestRequest(path, Method.Get);
        if (limit is not null) request.AddParameter("limit", limit.Value);
        return PaginationAsync(request, enumerateResponse, prepareNext, ct);
    }

    private async IAsyncEnumerable<TItem> PaginationAsync<TItem, TResponse>(RestRequest request, Func<TResponse, IEnumerable<TItem>> enumerateResponse, Func<TResponse, RestRequest, bool> prepareNext, [EnumeratorCancellation] CancellationToken ct = default) {
        TResponse? response;
        do {
            response = await client.GetAsync<TResponse>(request, ct);
            ArgumentNullException.ThrowIfNull(response);
            foreach (var item in enumerateResponse(response)) yield return item;
        } while (prepareNext(response, request));
    }

    public async Task<UserPresenceResponse> GetUserPresenceAsync(string userId, CancellationToken ct = default) {
        var response = await client.GetJsonAsync<UserPresenceResponse>($"/_matrix/client/v3/presence/{userId}/status", ct);
        ArgumentNullException.ThrowIfNull(response);
        return response;
    }

    public Task UpdateUserPresenceAsync(Presence presence, string? message = null, CancellationToken ct = default)
        => UpdateUserPresenceAsync(new() {
            Presence = presence,
            StatusMsg = message
        }, ct);
    public Task UpdateUserPresenceAsync(UserStatus status, CancellationToken ct = default)
        => client.PutJsonAsync($"/_matrix/client/v3/presence/{UserId}/status", status, ct);

    public async Task<string?> GetAvatarUrlAsync(string? userId = null, CancellationToken ct = default) {
        var response = await client.GetJsonAsync<AvatarUrlSingleton>($"/_matrix/client/v3/profile/{userId ?? UserId}/avatar_url", ct);
        ArgumentNullException.ThrowIfNull(response);
        return response.AvatarUrl;
    }
    public async Task SetAvatarUrlAsync(string? avatarUrl, string? userId = null, CancellationToken ct = default)
        => _ = await client.PutJsonAsync<AvatarUrlSingleton>($"/_matrix/client/v3/profile/{userId ?? UserId}/avatar_url", new() { AvatarUrl = avatarUrl }, ct);

    public async Task<string?> GetDisplayNameAsync(string? userId = null, CancellationToken ct = default) {
        var response = await client.GetJsonAsync<DisplayNameSingleton>($"/_matrix/client/v3/profile/{userId ?? UserId}/displayname", ct);
        ArgumentNullException.ThrowIfNull(response);
        return response.DisplayName;
    }
    public async Task SetDisplayNameAsync(string? displayName, string? userId = null, CancellationToken ct = default)
        => _ = await client.PutJsonAsync<DisplayNameSingleton>($"/_matrix/client/v3/profile/{userId ?? UserId}/displayname", new() { DisplayName = displayName }, ct);

    public IAsyncEnumerable<Notification> GetNotificationsAsync(CancellationToken ct = default)
        => PaginationAsync<Notification, FromToPaginationRequest<Notification>>(
            "/_matrix/client/v3/notifications",
            i => i.Notifications, (i, j) => i.PrepareNext(j),
            ct: ct);

    public async Task<IList<string>> GetSupportedLoginTypesAsync(CancellationToken ct = default) {
        var response = await client.GetJsonAsync<LoginFlowsResponse>("/_matrix/client/v3/login", ct);
        ArgumentNullException.ThrowIfNull(response);
        return response.Flows.Select(i => i.Type).ToList();
    }

    public async Task<IList<string>> GetJoinedRoomsAsync(CancellationToken ct = default) {
        var response = await client.GetJsonAsync<JoinedRoomsResponse>("/_matrix/client/v3/joined_rooms", ct);
        ArgumentNullException.ThrowIfNull(response);
        return response.JoinedRooms;
    }

    public async Task<string> JoinRoomAsync(string roomId, RoomJoinRequest? request = null, CancellationToken ct = default) {
        var response = await client.PostJsonAsync<RoomJoinRequest, RoomIdResponse>($"/_matrix/client/v3/rooms/{roomId}/join", request ?? new(), ct);
        ArgumentNullException.ThrowIfNull(response);
        return response.RoomId;
    }

    public void On<T>(Action<T> callback) where T : Event
        => On(typeof(T), callback);
    private void On<T>(Type type, Action<T> callback) {
        if (!callbacks.TryGetValue(type, out var cbList)) {
            cbList = new();
            callbacks.Add(type, cbList);
        }
        var callbackType = typeof(Action<>).MakeGenericType(type);
        if (!callback.GetType().IsAssignableTo(callbackType))
            throw new InvalidProgramException("Allowed bad callback!");
        cbList.Add(callback);
    }

    private readonly Dictionary<Type, List<object>> callbacks = new();
    private readonly MethodInfo callbackMethod = typeof(Client).GetMethod(nameof(InvokeCallback), BindingFlags.NonPublic | BindingFlags.Instance)
        ?? throw new InvalidProgramException("Callback function missing!");

    private void InvokeCallback<T>(T e) {
        var eventType = typeof(T);
        if (!callbacks.TryGetValue(eventType, out var cbList))
            Console.WriteLine($"Warning: Unhandled event of type {eventType.Name}!");
        else foreach (var cb in cbList.Cast<Action<T>>())
                cb(e);
    }
    private void Client_EventHappened(object? sender, Event e) {
        var method = callbackMethod.MakeGenericMethod(e.GetType());
        method.Invoke(this, new object[] { e });
    }
}
