﻿using System.Net;
using MatrixClient.Schema.Responses;

namespace MatrixClient;

public class ErrorHandler : DelegatingHandler {
    public ErrorHandler(HttpMessageHandler innerHandler) : base(innerHandler) { }
    protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken) {
        Console.WriteLine($"{request.Method.Method} request: {request.RequestUri}");
        if ((request.Method == HttpMethod.Post || request.Method == HttpMethod.Put)
            && request.Content is not null and var httpContent
            && await httpContent.ReadAsStringAsync(cancellationToken) is not null and var content)
            Console.WriteLine($"Content: {content}");
        var response = await base.SendAsync(request, cancellationToken);

        switch (response.StatusCode) {
        case HttpStatusCode.TooManyRequests:
            throw (await DeserializeResponseAsync<RateLimitResponse>(response, cancellationToken)).AsException(request);
        case var _ when !response.IsSuccessStatusCode:
            throw (await DeserializeResponseAsync<ApiErrorResponse>(response, cancellationToken)).AsException(request);
        default:
            var responseString = await response.Content.ReadAsStringAsync();
            Console.WriteLine($"Response: {responseString}");
            return response;
        }
    }

    private static async Task<T> DeserializeResponseAsync<T>(HttpResponseMessage response, CancellationToken ct) {
        var responseStream = await response.Content.ReadAsStreamAsync(ct);
        using var reader = new StreamReader(responseStream);
        using var jsonReader = new JsonTextReader(reader);
        var result = new JsonSerializer().Deserialize<T>(jsonReader);
        ArgumentNullException.ThrowIfNull(result);
        return result;
    }
}