﻿using RestSharp;

namespace MatrixClient.Schema.Responses;

public class PaginationResponse {
    /// <summary>
    /// A token that can be supplied to the to parameter of a pagination endpoint in order to retrieve later events. If no later events are available, this property may be omitted from the response.
    /// </summary>
    [JsonProperty("next_batch")] public string? NextBatch { get; set; }
    /// <summary>
    /// A token that can be supplied to the from parameter of a pagination endpoint in order to retrieve earlier events. If no earlier events are available, this property may be omitted from the response.
    /// </summary>
    [JsonProperty("prev_batch")] public string? PrevBatch { get; set; }
    public bool PrepareNext(RestRequest request) {
        if (NextBatch is null) return false;
        request.AddQueryParameter("since", NextBatch);
        return true;
    }
}