﻿namespace MatrixClient.Schema.Responses;

public class JoinedRoomsResponse {
    /// <summary>
    /// The ID of each room in which the user has joined membership.
    /// </summary>
    [JsonProperty("joined_rooms", Required = Required.Always)] public required IList<string> JoinedRooms { get; set; }
}
