﻿namespace MatrixClient.Schema.Responses;

public class LoginResponse {
    [JsonProperty("device_id")] public required string DeviceId { get; set; }
    [JsonProperty("access_token")] public required string AccessToken { get; set; }
    [JsonProperty("user_id")] public required string UserId { get; set; }
    [JsonProperty("expires_in_ms")] public int? ExpiresInMs { get; set; }
    [JsonProperty("refresh_token")] public string? RefreshToken { get; set; }
}
