﻿namespace MatrixClient.Schema.Responses;

public class RoomPaginationResponse : PaginationResponse {
    [JsonProperty("chunk")] public required IEnumerable<Room> Items { get; set; }
}