﻿using System.Collections;
using MatrixClient.Schema.Events;

namespace MatrixClient.Schema.Responses;

/// <summary>
/// Singleton wrapper for event lists.
/// </summary>
[JsonObject]
public class EventCollection<TEvent> : IEnumerable<TEvent> {
    /// <summary>
    /// List of events.
    /// </summary>
    [JsonProperty("events")] public required List<TEvent> Events { get; set; }
    public IEnumerator<TEvent> GetEnumerator() => Events.GetEnumerator();
    IEnumerator IEnumerable.GetEnumerator() => ((IEnumerable)Events).GetEnumerator();
}

/// <summary>
/// Wrapper for room sync events.
/// </summary>
public class Rooms {
    /// <summary>
    /// The rooms that the user has been invited to, mapped as room ID to room information.
    /// </summary>
    [JsonProperty("invite")] public IDictionary<string, InvitedRoom>? Invite { get; set; }
    /// <summary>
    /// The rooms that the user has joined, mapped as room ID to room information.
    /// </summary>
    [JsonProperty("join")] public IDictionary<string, JoinedRoom>? Join { get; set; }
    /// <summary>
    /// The rooms that the user has knocked upon, mapped as room ID to room information.
    /// </summary>
    [JsonProperty("knock")] public IDictionary<string, KnockedRoom>? Knock { get; set; }
    /// <summary>
    /// The rooms that the user has left or been banned from, mapped as room ID to room information.
    /// </summary>
    [JsonProperty("leave")] public IDictionary<string, LeftRoom>? Leave { get; set; }
}

public class SyncResponse : PaginationResponse {
    /// <summary>
    /// The global private data created by this user.
    /// </summary>
    [JsonProperty("account_data")] public EventCollection<Event>? AccountData { get; set; }
    /// <summary>
    /// The updates to the presence status of other users.
    /// </summary>
    [JsonProperty("presence")] public EventCollection<PresenceEvent>? Presence { get; set; }
    /// <summary>
    /// The updates to the presence status of other users.
    /// </summary>
    [JsonProperty("rooms")] public Rooms? Rooms { get; set; }
}