﻿using MatrixClient.Exceptions;

namespace MatrixClient.Schema.Responses;

public class RateLimitResponse : ApiErrorResponse {
    [JsonProperty("retry_after_ms")] public required int RetryMs { get; set; }
    public override MatrixLevelException AsException(HttpRequestMessage request) => new RateLimitException("Rate limit reached!", request, ErrorCode, RetryMs);
}
