﻿namespace MatrixClient.Schema.Responses;

public class LoginFlowsResponse {
    /// <summary>
    /// The homeserver’s supported login types.
    /// </summary>
    [JsonProperty("flows")] public required IList<LoginFlow> Flows { get; set; }
}
