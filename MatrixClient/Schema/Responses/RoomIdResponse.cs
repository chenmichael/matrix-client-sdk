﻿namespace MatrixClient.Schema.Responses;

/// <summary>
/// The room has been joined. The joined room ID must be returned in the <see cref="RoomId"/> field.
/// </summary>
public class RoomIdResponse {
    /// <summary>
    /// The joined room ID.
    /// </summary>
    [JsonProperty("room_id")] public required string RoomId { get; set; }
}