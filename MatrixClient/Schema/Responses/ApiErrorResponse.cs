﻿using MatrixClient.Exceptions;

namespace MatrixClient.Schema.Responses;

public class ApiErrorResponse {
    [JsonProperty("errcode")] public required string ErrorCode { get; set; }
    [JsonProperty("error")] public string? Error { get; set; }
    public virtual MatrixLevelException AsException(HttpRequestMessage request) => new(Error ?? $"Failed with code '{ErrorCode}' (no message)!", request, ErrorCode);
}
