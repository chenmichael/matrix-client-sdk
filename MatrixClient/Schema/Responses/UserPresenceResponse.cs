﻿namespace MatrixClient.Schema.Responses;

public class UserPresenceResponse : UserStatus {
    /// <summary>
    /// Whether the user is currently active.
    /// </summary>
    [JsonProperty("currently_active")] public bool? CurrentlyActive { get; set; }
    /// <summary>
    /// The length of time in milliseconds since an action was performed by this user.
    /// </summary>
    [JsonProperty("last_active_ago")] public int? LastActiveAgo { get; set; }
}
