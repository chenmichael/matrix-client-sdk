﻿using MatrixClient.Schema.Events;
using MatrixClient.Schema.Responses;

namespace MatrixClient.Schema;

public class Timeline : PaginationResponse {
    /// <summary>
    /// List of events.
    /// </summary>
    [JsonProperty("events", Required = Required.Always)] public required IList<Event> Events { get; set; }
    /// <summary>
    /// True if the number of events returned was limited by the limit on the filter.
    /// </summary>
    [JsonProperty("limited")] public bool? Limited { get; set; }
}