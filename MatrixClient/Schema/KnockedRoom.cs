﻿using MatrixClient.Schema.Responses;

namespace MatrixClient.Schema;

public class KnockedRoom {
    /// <summary>
    /// The stripped state of a room that the user has knocked upon.
    /// </summary>
    [JsonProperty("knock_state")] public EventCollection<StrippedStateEvent>? KnockState { get; set; }
}