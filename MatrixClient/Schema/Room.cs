﻿namespace MatrixClient.Schema;

public class Room {
    /// <summary>
    /// The URL for the room's avatar, if one is set.
    /// </summary>
    [JsonProperty("avatar_url")] public string? AvatarUrl { get; set; }
    /// <summary>
    /// The canonical alias of the room, if any.
    /// </summary>
    [JsonProperty("canonical_alias")] public string? CanonicalAlias { get; set; }
    /// <summary>
    /// Whether guest users may join the room and participate in it. If they can, they will be subject to ordinary power level rules like any other user.
    /// </summary>
    [JsonProperty("guest_can_join")] public required bool GuestCanJoin { get; set; }
    /// <summary>
    /// The room's join rule. When not present, the room is assumed to be public. Note that rooms with invite join rules are not expected here, but rooms with knock rules are given their near-public nature.
    /// </summary>
    [JsonProperty("join_rule")] public string? JoinRule { get; set; }
    /// <summary>
    /// The name of the room, if any.
    /// </summary>
    [JsonProperty("name")] public string? Name { get; set; }
    /// <summary>
    /// The number of members joined to the room.
    /// </summary>
    [JsonProperty("num_joined_members")] public required int NumJoinedMembers { get; set; }
    /// <summary>
    /// The ID of the room.
    /// </summary>
    [JsonProperty("room_id")] public required string RoomId { get; set; }
    /// <summary>
    /// The type of room (from m.room.create), if any.
    /// </summary>
    [JsonProperty("room_type")] public string? RoomType { get; set; }
    /// <summary>
    /// The topic of the room, if any.
    /// </summary>
    [JsonProperty("topic")] public string? Topic { get; set; }
    /// <summary>
    /// Whether the room may be viewed by guest users without joining.
    /// </summary>
    [JsonProperty("world_readable")] public required bool WorldReadable { get; set; }
}