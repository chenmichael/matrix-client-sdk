﻿namespace MatrixClient.Schema.Id;

[JsonConverter(typeof(JsonKnownTypesConverter<Identifier>))]
[JsonDiscriminator(Name = "type")]
[JsonKnownType(typeof(User), User.Type)]
public abstract class Identifier { }
