﻿namespace MatrixClient.Schema.Id;

/// <summary>
/// A client can identify a user using their Matrix ID.
/// </summary>
public class User : Identifier {
    public const string Type = "m.id.user";
    /// <summary>
    /// This can either be the fully qualified Matrix user ID, or just the localpart of the user ID.
    /// </summary>
    [JsonProperty("user")] public required string UserId { get; set; }
}
