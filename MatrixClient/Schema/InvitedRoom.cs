﻿namespace MatrixClient.Schema;

public class InvitedRoom {
    /// <summary>
    /// The stripped state of a room that the user has been invited to.
    /// </summary>
    [JsonProperty("invite_state")] public InviteState? InviteState { get; set; }
}
