﻿using MatrixClient.Schema.Events;

namespace MatrixClient.Schema;

public class StrippedStateEvent {
    /// <summary>
    /// The content for the event.
    /// </summary>
    [JsonProperty("content", Required = Required.Always)] public required EventContent Content { get; set; }
    /// <summary>
    /// The sender for the event.
    /// </summary>
    [JsonProperty("sender", Required = Required.Always)] public required string Sender { get; set; }
    /// <summary>
    /// The state_key for the event.
    /// </summary>
    [JsonProperty("state_key", Required = Required.Always)] public required string StateKey { get; set; }
    /// <summary>
    /// The type for the event.
    /// </summary>
    [JsonProperty("type", Required = Required.Always)] public required string Type { get; set; }
}
