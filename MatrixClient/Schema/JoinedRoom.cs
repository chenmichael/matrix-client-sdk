﻿using MatrixClient.Schema.Events;
using MatrixClient.Schema.Responses;

namespace MatrixClient.Schema;

public class JoinedRoom {
    /// <summary>
    /// The private data that this user has attached to this room.
    /// </summary>
    [JsonProperty("account_data")] public AccountData? AccountData { get; set; }
    /// <summary>
    /// The new ephemeral events in the room (events that aren’t recorded in the timeline or state of the room). In this version of the spec, these are typing notification and read receipt events.
    /// </summary>
    [JsonProperty("ephemeral")] public EventCollection<Event>? Ephemeral { get; set; }
    /// <summary>
    /// Updates to the state, between the time indicated by the since parameter, and the start of the timeline (or all state up to the start of the timeline, if since is not given, or full_state is true). N.B. state updates for m.room.member events will be incomplete if lazy_load_members is enabled in the /sync filter, and only return the member events required to display the senders of the timeline events in this response. summary RoomSummary     Information about the room which clients may need to correctly render it to users.
    /// </summary>
    //[JsonProperty("state")] public EventCollection<ClientEventWithoutRoomID>? State { get; set; }
    /// <summary>
    /// The timeline of messages and state changes in the room.
    /// </summary>
    [JsonProperty("timeline")] public Timeline? Timeline { get; set; }
    /// <summary>
    /// Counts of unread notifications for this room. See the Receiving notifications section for more information on how these are calculated. If unread_thread_notifications was specified as true on the RoomEventFilter, these counts will only be for the main timeline rather than all events in the room. See the threading module for more information. Changed in v1.4: Updated to reflect behaviour of having unread_thread_notifications as true in the RoomEventFilter for /sync.
    /// </summary>
    [JsonProperty("unread_notifications")] public NotificationCounts? UnreadNotifications { get; set; }
    /// <summary>
    /// If unread_thread_notifications was specified as true on the RoomEventFilter, the notification counts for each thread in this room. The object is keyed by thread root ID, with values matching unread_notifications. If a thread does not have any notifications it can be omitted from this object. If no threads have notification counts, this whole object can be omitted. Added in v1.4
    /// </summary>
    [JsonProperty("unread_thread_notifications")] public IDictionary<string, NotificationCounts>? UnreadThreadNotifications { get; set; }
}
