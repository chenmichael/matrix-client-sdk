﻿namespace MatrixClient.Schema;

public class DisplayNameSingleton {
    [JsonProperty("displayname")] public string? DisplayName { get; set; }
}