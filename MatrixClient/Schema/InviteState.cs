﻿namespace MatrixClient.Schema;

public class InviteState {
    /// <summary>
    /// The <see href="https://spec.matrix.org/v1.5/client-server-api/#stripped-state">stripped state events</see> that form the invite state.
    /// </summary>
    [JsonProperty("events")] public IList<StrippedStateEvent>? Events { get; set; }
}
