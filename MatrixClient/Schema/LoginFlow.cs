﻿namespace MatrixClient.Schema;

public class LoginFlow {
    /// <summary>
    /// The login type. This is supplied as the type when logging in.
    /// </summary>
    [JsonProperty("type")] public required string Type { get; set; }
}
