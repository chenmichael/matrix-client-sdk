﻿namespace MatrixClient.Schema;

public class AvatarUrlSingleton {
    [JsonProperty("avatar_url")] public string? AvatarUrl { get; set; }
}