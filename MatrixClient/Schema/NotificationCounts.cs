﻿namespace MatrixClient.Schema;

public class NotificationCounts {
    /// <summary>
    /// The number of unread notifications for this room with the highlight flag set.
    /// </summary>
    [JsonProperty("highlight_count")] public int HighlightCount { get; set; }
    /// <summary>
    /// The total number of unread notifications for this room.
    /// </summary>
    [JsonProperty("notification_count")] public int NotificationCount { get; set; }
}
