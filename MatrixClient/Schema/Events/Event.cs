﻿using MatrixClient.Schema.Events.Room;

namespace MatrixClient.Schema.Events;

/// <summary>
/// The format used for events when they are returned from a homeserver to a client via the Client-Server API, or sent to an Application Service via the Application Services API.
/// <see href="https://spec.matrix.org/v1.5/client-server-api/#events">Matrix Specification</see>
/// </summary>
[JsonConverter(typeof(JsonKnownTypesConverter<Event>))]
[JsonDiscriminator(Name = "type")]
[JsonKnownTypeFallback(typeof(UnknownEvent))]
[JsonKnownType(typeof(TypingEvent), TypingEvent.Type)]
[JsonKnownType(typeof(RoomPowerLevelsEvent), RoomPowerLevelsEvent.Type)]
[JsonKnownType(typeof(RoomMessageEvent), RoomMessageEvent.Type)]
[JsonKnownType(typeof(RoomMemberEvent), RoomMemberEvent.Type)]
[JsonKnownType(typeof(PushRulesEvent), PushRulesEvent.Type)]
[JsonKnownType(typeof(RoomGuestAccessEvent), RoomGuestAccessEvent.Type)]
[JsonKnownType(typeof(RoomNameEvent), RoomNameEvent.Type)]
[JsonKnownType(typeof(RoomJoinRulesEvent), RoomJoinRulesEvent.Type)]
[JsonKnownType(typeof(RoomHistoryVisibilityEvent), RoomHistoryVisibilityEvent.Type)]
[JsonKnownType(typeof(RoomCreateEvent), RoomCreateEvent.Type)]
[JsonKnownType(typeof(RoomEncryptedEvent), RoomEncryptedEvent.Type)]
[JsonKnownType(typeof(PresenceEvent), PresenceEvent.Type)]
[JsonKnownType(typeof(ReceiptEvent), ReceiptEvent.Type)]
[JsonKnownType(typeof(DirectEvent), DirectEvent.Type)]
public abstract class Event { }

public abstract class Event<T> : Event, IEventContent<T> {
    /// <inheritdoc cref="IEventContent{T}.Content"/>
    [JsonProperty("content")] public required T Content { get; set; }
}
