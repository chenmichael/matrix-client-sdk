﻿namespace MatrixClient.Schema.Events;

public class TypingEvent : Event<TypingEvent.Args> {
    public const string Type = "m.typing";
    public class Args {
        /// <summary>
        /// The list of user IDs typing in this room, if any.
        /// </summary>
        [JsonProperty("user_ids", Required = Required.Always)] public required IList<string> UserIds { get; set; }
    }
}
