﻿namespace MatrixClient.Schema.Events;

/// <summary>
/// Informs the client of new receipts.
/// Changed in v1.4: Added m.read.private receipts to the event’s content.
/// <see href="https://spec.matrix.org/v1.5/client-server-api/#mreceipt">Matrix Specification</see>
/// </summary>
public class ReceiptEvent : Event<ReceiptEvent.Args> {
    public const string Type = "m.receipt";
    /// <summary>
    /// TODO: Receipt event content.
    /// </summary>
    public class Args { }
}
