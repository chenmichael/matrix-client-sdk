﻿namespace MatrixClient.Schema.Events;

public class UnknownEvent : Event {
    [JsonProperty("type", Required = Required.Always)] public required string Type { get; set; }
}