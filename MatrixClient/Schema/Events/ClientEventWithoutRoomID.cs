﻿using MatrixClient.Serialization;

namespace MatrixClient.Schema.Events;

public class ClientEventWithoutRoomID : Event {
    /// <summary>
    /// Timestamp (in milliseconds since the unix epoch) on originating homeserver when this event was sent.
    /// </summary>
    [JsonConverter(typeof(UnixMillisecondsConverter))]
    [JsonProperty("origin_server_ts", Required = Required.Always)] public required DateTime OriginServerTimestamp { get; set; }
    /// <summary>
    /// The globally unique identifier for this event.
    /// </summary>
    [JsonProperty("event_id", Required = Required.Always)] public required string EventId { get; set; }
    /// <summary>
    /// Contains the fully-qualified ID of the user who sent this event.
    /// </summary>
    [JsonProperty("sender", Required = Required.Always)] public required string Sender { get; set; }
    /// <summary>
    /// Present if, and only if, this event is a state event. The key making this piece of state unique in the room. Note that it is often an empty string. State keys starting with an @ are reserved for referencing user IDs, such as room members. With the exception of a few events, state events set with a given user’s ID as the state key MUST only be set by that user.
    /// </summary>
    [JsonProperty("state_key")] public string? StateKey { get; set; }
    /// <summary>
    /// Contains optional extra information about the event.
    /// </summary>
    [JsonProperty("unsigned")] public UnsignedData? Unsigned { get; set; }
}

public abstract class ClientEventWithoutRoomID<T> : ClientEventWithoutRoomID, IEventContent<T> {
    /// <inheritdoc cref="IEventContent{T}.Content"/>
    [JsonProperty("content")] public required T Content { get; set; }
}
