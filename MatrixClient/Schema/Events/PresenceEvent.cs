﻿using MatrixClient.Schema.Enums;

namespace MatrixClient.Schema.Events;

public class PresenceEvent : Event<PresenceEvent.Args> {
    public const string Type = "m.presence";
    public class Args {
        /// <summary>
        /// The current avatar URL for this user, if any.
        /// </summary>
        [JsonProperty("avatar_url")] public string? AvatarUrl { get; set; }
        /// <summary>
        /// Whether the user is currently active
        /// </summary>
        [JsonProperty("currently_active")] public bool? CurrentlyActive { get; set; }
        /// <summary>
        /// The current display name for this user, if any.
        /// </summary>
        [JsonProperty("displayname")] public string? Displayname { get; set; }
        /// <summary>
        /// The last time since this used performed some action, in milliseconds.
        /// </summary>
        [JsonProperty("last_active_ago")] public int? LastActiveAgo { get; set; }
        /// <summary>
        /// The presence state for this user.
        /// </summary>
        [JsonProperty("presence", Required = Required.Always)] public required Presence Presence { get; set; }
        /// <summary>
        /// An optional description to accompany the presence.
        /// </summary>
        [JsonProperty("status_msg")] public string? StatusMsg { get; set; }
    }
}
