﻿using MatrixClient.Serialization;

namespace MatrixClient.Schema.Events;

public class Notification {
    /// <summary>
    /// The Event object for the event that triggered the notification.
    /// </summary>
    [JsonProperty("event", Required = Required.Always)] public required Event Event { get; set; }
    /// <summary>
    /// The profile tag of the rule that matched this event.
    /// </summary>
    [JsonProperty("profile_tag")] public string? ProfileTag { get; set; }
    /// <summary>
    /// Indicates whether the user has sent a read receipt indicating that they have read this message.
    /// </summary>
    [JsonProperty("read", Required = Required.Always)] public required bool Read { get; set; }
    /// <summary>
    /// The ID of the room in which the event was posted.
    /// </summary>
    [JsonProperty("room_id", Required = Required.Always)] public required string RoomId { get; set; }
    /// <summary>
    /// The unix timestamp at which the event notification was sent..
    /// </summary>
    [JsonConverter(typeof(UnixMillisecondsConverter))]
    [JsonProperty("ts", Required = Required.Always)] public required DateTime Ts { get; set; }
}
