﻿using MatrixClient.Schema.Enums;

namespace MatrixClient.Schema.Events;

public class EventContent {
    /// <summary>
    /// The avatar URL for this user, if any.
    /// </summary>
    [JsonProperty("avatar_url")] public string? AvatarUrl { get; set; }
    /// <summary>
    /// The display name for this user, if any.
    /// </summary>
    [JsonProperty("displayname")] public string? Displayname { get; set; }
    /// <summary>
    /// Flag indicating if the room containing this event was created with the intention of being a direct chat. See Direct Messaging.
    /// </summary>
    [JsonProperty("is_direct")] public bool? IsDirect { get; set; }
    /// <summary>
    /// Usually found on join events, this field is used to denote which homeserver (through representation of a user with sufficient power level) authorised the user’s join. More information about this field can be found in the Restricted Rooms Specification. Client and server implementations should be aware of the signing implications of including this field in further events: in particular, the event must be signed by the server which owns the user ID in the field. When copying the membership event’s content (for profile updates and similar) it is therefore encouraged to exclude this field in the copy, as otherwise the event might fail event authorization. Added in v1.2
    /// </summary>
    [JsonProperty("join_authorised_via_users_server")] public string? JoinAuthorisedViaUsersServer { get; set; }
    /// <summary>
    /// The membership state of the user.
    /// </summary>
    [JsonProperty("membership")] public Membership? Membership { get; set; }
    /// <summary>
    /// Optional user-supplied text for why their membership has changed. For kicks and bans, this is typically the reason for the kick or ban. For other membership changes, this is a way for the user to communicate their intent without having to send a message to the room, such as in a case where Bob rejects an invite from Alice about an upcoming concert, but can’t make it that day. Clients are not recommended to show this reason to users when receiving an invite due to the potential for spam and abuse. Hiding the reason behind a button or other component is recommended. Added in v1.1
    /// </summary>
    [JsonProperty("reason")] public string? Reason { get; set; }
    /// <summary>
    /// Third party invitation to the room.
    /// </summary>
    [JsonProperty("third_party_invite")] public Invite? ThirdPartyInvite { get; set; }
}
