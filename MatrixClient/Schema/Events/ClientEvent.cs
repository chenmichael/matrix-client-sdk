﻿namespace MatrixClient.Schema.Events;

public abstract class ClientEvent : ClientEventWithoutRoomID {
    /// <summary>
    /// The ID of the room associated with this event.
    /// </summary>
    [JsonProperty("room_id", Required = Required.Always)] public required string RoomId { get; set; }
}

public abstract class ClientEvent<T> : ClientEvent, IEventContent<T> {
    /// <inheritdoc cref="IEventContent{T}.Content"/>
    [JsonProperty("content")] public required T Content { get; set; }
}
