﻿namespace MatrixClient.Schema.Events.Room;

/// <summary>
/// This event specifies the minimum level a user must have in order to perform a certain action. It also specifies the levels of each user in the room.
/// <see href="https://spec.matrix.org/v1.5/client-server-api/#mroompower_levels">Matrix Specification</see>
/// </summary>
public class RoomPowerLevelsEvent : ClientEventWithoutRoomID<RoomPowerLevelsEvent.Args> {
    public const string Type = "m.room.power_levels";
    public class Args {
        /// <summary>
        /// The level required to ban a user. Defaults to 50 if unspecified.
        /// </summary>
        [JsonProperty("ban")] public int Ban { get; set; } = 50;
        /// <summary>
        /// The level required to send specific event types. This is a mapping from event type to power level required.
        /// </summary>
        [JsonProperty("events")] public Dictionary<string, int>? Events { get; set; }
        /// <summary>
        /// The default level required to send message events. Can be overridden by the events key. Defaults to 0 if unspecified.
        /// </summary>
        [JsonProperty("events_default")] public int EventsDefault { get; set; } = 0;
        /// <summary>
        /// The level required to invite a user. Defaults to 0 if unspecified.
        /// </summary>
        [JsonProperty("invite")] public int Invite { get; set; } = 0;
        /// <summary>
        /// The level required to kick a user. Defaults to 50 if unspecified.
        /// </summary>
        [JsonProperty("kick")] public int Kick { get; set; } = 50;
        /// <summary>
        /// The power level requirements for specific notification types. This is a mapping from key to power level for that notifications key.
        /// </summary>
        [JsonProperty("notifications")] public NotificationsLevel? Notifications { get; set; }
        /// <summary>
        /// The level required to redact an event sent by another user. Defaults to 50 if unspecified.
        /// </summary>
        [JsonProperty("redact")] public int Redact { get; set; } = 50;
        /// <summary>
        /// The default level required to send state events. Can be overridden by the events key. Defaults to 50 if unspecified.
        /// </summary>
        [JsonProperty("state_default")] public int StateDefault { get; set; } = 50;
        /// <summary>
        /// The power levels for specific users. This is a mapping from user_id to power level for that user.
        /// </summary>
        [JsonProperty("users")] public Dictionary<string, int>? Users { get; set; }
        /// <summary>
        /// The default power level for every user in the room, unless their user_id is mentioned in the users key. Defaults to 0 if unspecified.
        /// </summary>
        [JsonProperty("users_default")] public int UsersDefault { get; set; } = 0;
        public class NotificationsLevel {
            /// <summary>
            /// The level required to trigger an @room notification. Defaults to 50 if unspecified.
            /// </summary>
            [JsonProperty("room")] public int? Room { get; set; }
        }
    }
}
