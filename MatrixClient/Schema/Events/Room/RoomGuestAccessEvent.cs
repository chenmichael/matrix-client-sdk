﻿namespace MatrixClient.Schema.Events.Room;

public class RoomGuestAccessEvent : ClientEventWithoutRoomID<RoomGuestAccessEvent.Args> {
    public const string Type = "m.room.guest_access";
    public class Args {
        [JsonProperty("guest_access")] public required string GuestAccess { get; set; }
    }
}
