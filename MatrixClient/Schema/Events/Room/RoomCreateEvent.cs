﻿namespace MatrixClient.Schema.Events.Room;

public class RoomCreateEvent : ClientEventWithoutRoomID<RoomCreateEvent.Args> {
    public const string Type = "m.room.create";
    public class Args {
        /// <summary>
        /// The user_id of the room creator. This is set by the homeserver.
        /// </summary>
        [JsonProperty("creator", Required = Required.Always)] public required string Creator { get; set; }
        /// <summary>
        /// Whether users on other servers can join this room. Defaults to true if key does not exist.
        /// </summary>
        [JsonProperty("m.federate")] public bool MFederate { get; set; } = true;
        /// <summary>
        /// A reference to the room this room replaces, if the previous room was upgraded.
        /// </summary>
        [JsonProperty("predecessor")] public PreviousRoom? Predecessor { get; set; }
        /// <summary>
        /// The version of the room. Defaults to "1" if the key does not exist.
        /// </summary>
        [JsonProperty("room_version")] public string RoomVersion { get; set; } = "1";
        /// <summary>
        /// Optional <see href="https://spec.matrix.org/v1.5/client-server-api/#types">room type</see> to denote a room’s intended function outside of traditional conversation. Unspecified room types are possible using <see href="https://spec.matrix.org/v1.5/appendices/#common-namespaced-identifier-grammar">Namespaced Identifiers</see>.
        /// </summary>
        [JsonProperty("type")] public string? Type { get; set; }
    }
}
