﻿using MatrixClient.Schema.Enums;

namespace MatrixClient.Schema.Events.Room;

/// <summary>
/// This event specifies the minimum level a user must have in order to perform a certain action. It also specifies the levels of each user in the room.
/// <see href="https://spec.matrix.org/v1.5/client-server-api/#mroompower_levels">Matrix Specification</see>
/// </summary>
public class RoomEncryptedEvent : ClientEventWithoutRoomID<RoomEncryptedEvent.Args> {
    public const string Type = "m.room.encrypted";
    public class Args {
        /// <summary>
        /// The encryption algorithm used to encrypt this event. The value of this field determines which other properties will be present.
        /// </summary>
        [JsonProperty("algorithm", Required = Required.Always)] public required EncryptionAlgorithm Algorithm { get; set; }
        /// <summary>
        /// The encrypted content of the event. Either the encrypted payload itself, in the case of a Megolm event, or a map from the recipient Curve25519 identity key to ciphertext information, in the case of an Olm event. For more details, see Messaging Algorithms.
        /// </summary>
        [JsonProperty("ciphertext", Required = Required.Always)] public required string Ciphertext { get; set; }
        /// <summary>
        /// The ID of the sending device. Deprecated: This field provides no additional security or privacy benefit for Megolm messages and must not be read from if the encrypted event is using Megolm. It should still be included on outgoing messages, however must not be used to find the corresponding session. See m.megolm.v1.aes-sha2 for more information. Changed in v1.3: Previously this field was required for Megolm messages, however given it offers no additional security or privacy benefit it has been deprecated for Megolm messages. See m.megolm.v1.aes-sha2 for more information.
        /// </summary>
        [JsonProperty("device_id")] public string? DeviceId { get; set; }
        /// <summary>
        /// The Curve25519 key of the sender. Required (not deprecated) if not using Megolm. Deprecated: This field provides no additional security or privacy benefit for Megolm messages and must not be read from if the encrypted event is using Megolm. It should still be included on outgoing messages, however must not be used to find the corresponding session. See m.megolm.v1.aes-sha2 for more information. Changed in v1.3: Previously this field was required, however given it offers no additional security or privacy benefit it has been deprecated for Megolm messages. See m.megolm.v1.aes-sha2 for more information.
        /// </summary>
        [JsonProperty("sender_key")] public string? SenderKey { get; set; }
        /// <summary>
        /// The ID of the session used to encrypt the message. Required with Megolm.
        /// </summary>
        [JsonProperty("session_id")] public string? SessionId { get; set; }
    }
}
