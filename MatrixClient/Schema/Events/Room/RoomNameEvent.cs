﻿namespace MatrixClient.Schema.Events.Room;

/// <summary>
/// A room has an opaque room ID which is not human-friendly to read. A room alias is human-friendly, but not all rooms have room aliases. The room name is a human-friendly string designed to be displayed to the end-user. The room name is not unique, as multiple rooms can have the same room name set.
/// A room with an m.room.name event with an absent, null, or empty name field should be treated the same as a room with no m.room.name event.
/// An event of this type is automatically created when creating a room using /createRoom with the name key.
/// </summary>
public class RoomNameEvent : ClientEvent<RoomNameEvent.Args> {
    public const string Type = "m.room.name";
    public class Args {
        /// <summary>
        /// The name of the room.
        /// </summary>
        [JsonProperty("name", Required = Required.Always)] public required string Name { get; set; }
    }
}
