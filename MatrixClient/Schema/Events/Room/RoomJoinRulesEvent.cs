﻿using MatrixClient.Schema.Enums;

namespace MatrixClient.Schema.Events.Room;

/// <summary>
/// A room may be assigned different designations (see <see cref="JoinRule"/>).
/// </summary>
public class RoomJoinRulesEvent : ClientEventWithoutRoomID<RoomJoinRulesEvent.Args> {
    public const string Type = "m.room.join_rules";
    public class Args {
        /// <summary>
        /// For restricted rooms, the conditions the user will be tested against. The user needs only to satisfy one of the conditions to join the restricted room. If the user fails to meet any condition, or the condition is unable to be confirmed as satisfied, then the user requires an invite to join the room. Improper or no allow conditions on a restricted join rule imply the room is effectively invite-only (no conditions can be satisfied). Added in v1.2
        /// </summary>
        [JsonProperty("allow")] public IList<AllowCondition>? Allow { get; set; }
        /// <summary>
        /// The type of rules used for users wishing to join this room.
        /// </summary>
        [JsonProperty("join_rule", Required = Required.Always)] public required JoinRule JoinRule { get; set; }
    }
}
