﻿namespace MatrixClient.Schema.Events.Room;

public class RoomMemberEvent : ClientEventWithoutRoomID<RoomMemberEvent.Args> {
    public const string Type = "m.room.member";
    public const string Invite = "invite";
    public const string Leave = "leave";
    public class Args {
        [JsonProperty("is_direct")] public bool IsDirect { get; set; }
        [JsonProperty("membership")] public required string Membership { get; set; }
        [JsonProperty("displayname")] public string? DisplayName { get; set; }
        [JsonProperty("avatar_url")] public string? AvatarUrl { get; set; }
    }
}
