﻿namespace MatrixClient.Schema.Events.Room;

public class RoomHistoryVisibilityEvent : ClientEventWithoutRoomID<RoomHistoryVisibilityEvent.Args> {
    public const string Type = "m.room.history_visibility";
    public class Args {
        [JsonProperty("history_visibility")] public required string HistoryVisibility { get; set; }
    }
}
