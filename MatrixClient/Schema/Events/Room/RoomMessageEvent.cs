﻿using MatrixClient.Schema.Messages;

namespace MatrixClient.Schema.Events.Room;

/// <summary>
/// This event is used when sending messages in a room. Messages are not limited to be text. The msgtype key outlines the type of message, e.g. text, audio, image, video, etc. The body key is text and MUST be used with every kind of msgtype as a fallback mechanism for when a client cannot render a message. This allows clients to display something even if it is just plain text.
/// </summary>
public class RoomMessageEvent : ClientEventWithoutRoomID<Message> {
    public const string Type = "m.room.message";
}
