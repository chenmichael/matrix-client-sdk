﻿namespace MatrixClient.Schema.Events;

public interface IEventContent<T> {
    /// <summary>
    /// The body of this event, as created by the client which sent it.
    /// </summary>
    [JsonProperty("content")] T Content { get; set; }
}