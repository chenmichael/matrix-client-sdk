﻿namespace MatrixClient.Schema.Events;

/// <summary>
/// A map of which rooms are considered ‘direct’ rooms for specific users is kept in account_data in an event of type m.direct. The content of this event is an object where the keys are the user IDs and values are lists of room ID strings of the ‘direct’ rooms for that user ID.
/// </summary>
public class DirectEvent : Event<IDictionary<string, IList<string>>> {
    public const string Type = "m.direct";
}
