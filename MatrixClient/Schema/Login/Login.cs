﻿using MatrixClient.Schema.Id;

namespace MatrixClient.Schema.Login;

public abstract class Login {
    [JsonProperty("type")] public abstract string Type { get; }
    [JsonProperty("identifier")] public required Identifier Identifier { get; set; }
    [JsonProperty("device_id")] public required string DeviceId { get; set; }
}
