﻿namespace MatrixClient.Schema.Login;

public class TokenLogin : Login {
    public override string Type => "m.login.token";
    [JsonProperty("token")] public required string Token { get; set; }
}
