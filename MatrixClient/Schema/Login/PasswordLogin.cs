﻿namespace MatrixClient.Schema.Login;

public class PasswordLogin : Login {
    public override string Type => "m.login.password";
    [JsonProperty("password")] public required string Password { get; set; }
}
