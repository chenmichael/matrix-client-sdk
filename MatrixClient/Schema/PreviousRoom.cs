﻿
namespace MatrixClient.Schema;

public class PreviousRoom {
    /// <summary>
    /// The event ID of the last known event in the old room.
    /// </summary>
    [JsonProperty("event_id", Required = Required.Always)] public required string EventId { get; set; }
    /// <summary>
    /// The ID of the old room.
    /// </summary>
    [JsonProperty("room_id", Required = Required.Always)] public required string RoomId { get; set; }
}

