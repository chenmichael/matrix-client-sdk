﻿namespace MatrixClient.Schema;

public class RoomJoinRequest {
    /// <summary>
    /// Optional reason to be included as the reason on the subsequent membership event.
    /// </summary>
    [JsonProperty("reason")] public string? Reason { get; set; }
    /// <summary>
    /// If supplied, the homeserver must verify that it matches a pending m.room.third_party_invite event in the room, and perform key validity checking if required by the event.
    /// </summary>
    [JsonProperty("third_party_signed")] public Invite? ThirdPartySigned { get; set; }
}

public class Invite {
    /// <summary>
    /// The Matrix ID of the invitee.
    /// </summary>
    [JsonProperty("mxid")] public required string Invitee { get; set; }
    /// <summary>
    /// The Matrix ID of the user who issued the invite.
    /// </summary>
    [JsonProperty("sender")] public required string Sender { get; set; }
    /// <summary>
    /// A signatures object containing a signature of the entire signed object.
    /// </summary>
    [JsonProperty("signatures")] public required Dictionary<string, Dictionary<string, string>> Signatures { get; set; }
    /// <summary>
    /// The state key of the m.third_party_invite event.
    /// </summary>
    [JsonProperty("token")] public required string Token { get; set; }
}
