﻿namespace MatrixClient.Schema;

public class AllowCondition {
    /// <summary>
    /// Required if type is m.room_membership. The room ID to check the user’s membership against. If the user is joined to this room, they satisfy the condition and thus are permitted to join the restricted room.
    /// </summary>
    [JsonProperty("room_id")] public string? RoomId { get; set; }
    /// <summary>
    /// The type of condition: m.room_membership - the user satisfies the condition if they are joined to the referenced room.
    /// </summary>
    [JsonProperty("type", Required = Required.Always)] public required string Type { get; set; }
}
