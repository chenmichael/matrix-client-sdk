﻿using MatrixClient.Schema.Events;
using MatrixClient.Schema.Responses;

namespace MatrixClient.Schema;

public class LeftRoom {
    /// <summary>
    /// The private data that this user has attached to this room.
    /// </summary>
    [JsonProperty("account_data")] public EventCollection<Event>? AccountData { get; set; }
    /// <summary>
    /// The state updates for the room up to the start of the timeline.
    /// </summary>
    [JsonProperty("state")] public State? State { get; set; }
    /// <summary>
    /// The timeline of messages and state changes in the room up to the point when the user left.
    /// </summary>
    [JsonProperty("timeline")] public Timeline? Timeline { get; set; }
}
