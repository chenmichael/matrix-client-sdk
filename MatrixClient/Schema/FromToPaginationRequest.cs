﻿using RestSharp;

namespace MatrixClient.Schema;

public class FromToPaginationRequest<T> {
    /// <summary>
    /// The token to supply in the from param of the next /notifications request in order to request more events. If this is absent, there are no more results.
    /// </summary>
    [JsonProperty("next_token")] public string? NextToken { get; set; }
    /// <summary>
    /// The list of events that triggered notifications.
    /// </summary>
    [JsonProperty("notifications")] public required IEnumerable<T> Notifications { get; set; }
    public bool PrepareNext(RestRequest request) {
        if (NextToken is null) return false;
        request.AddQueryParameter("from", NextToken);
        return true;
    }
}