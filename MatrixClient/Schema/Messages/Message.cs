﻿namespace MatrixClient.Schema.Messages;

[JsonConverter(typeof(JsonKnownTypesConverter<Message>))]
[JsonDiscriminator(Name = "msgtype")]
[JsonKnownType(typeof(TextMessage), TextMessage.Type)]
public abstract class Message { }
