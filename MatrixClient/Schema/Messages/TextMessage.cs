﻿namespace MatrixClient.Schema.Messages;

public class TextMessage : Message {
    public const string Type = "m.text";
    [JsonProperty("body")] public string? Body { get; set; }
    [JsonProperty("org.matrix.msc1767.text")] public string? Msc1767Text { get; set; }
}
