﻿using MatrixClient.Schema.Enums;

namespace MatrixClient.Schema;

public class UserStatus {
    /// <summary>
    /// This user's presence.
    /// </summary>
    [JsonProperty("presence")] public required Presence Presence { get; set; }
    /// <summary>
    /// The state message for this user if one was set.
    /// </summary>
    [JsonProperty("status_msg")] public string? StatusMsg { get; set; }
}