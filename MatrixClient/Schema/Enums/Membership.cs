﻿using System.Runtime.Serialization;

namespace MatrixClient.Schema.Enums;

/// <summary>
/// Users need to be a member of a room in order to send and receive events in that room. There are several states in which a user may be, in relation to a room, represented by this enumeration.
/// <see href="https://spec.matrix.org/v1.5/client-server-api/#room-membership">Matrix Specification</see>
/// </summary>
[JsonConverter(typeof(StringEnumConverter))]
public enum Membership {
    /// <summary>
    /// The user has been invited to participate in the room, but is not yet participating
    /// </summary>
    [EnumMember(Value = "invite")] Invite,
    /// <summary>
    /// The user can send and receive events in the room
    /// </summary>
    [EnumMember(Value = "join")] Join,
    /// <summary>
    /// The user has requested to participate in the room, but has not yet been allowed to
    /// </summary>
    [EnumMember(Value = "knock")] Knock,
    /// <summary>
    /// The user cannot send or receive events in the room
    /// </summary>
    [EnumMember(Value = "leave")] Leave,
    /// <summary>
    /// The user is not allowed to join the room.
    /// </summary>
    [EnumMember(Value = "ban")] Ban,
}
