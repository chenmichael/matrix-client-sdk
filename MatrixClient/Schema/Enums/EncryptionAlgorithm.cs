﻿namespace MatrixClient.Schema.Enums;

[JsonConverter(typeof(StringEnumConverter))]
public enum EncryptionAlgorithm {
    [EnumMember(Value = "m.olm.v1.curve25519-aes-sha2")] OlmV1Curve25519AesSha2,
    [EnumMember(Value = "m.megolm.v1.aes-sha2")] MegolmV1AesSha2,
}
