﻿using System.Runtime.Serialization;

namespace MatrixClient.Schema.Enums;

[JsonConverter(typeof(StringEnumConverter))]
public enum Presence {
    [EnumMember(Value = "online")] Online,
    [EnumMember(Value = "offline")] Offline,
    [EnumMember(Value = "unavailable")] Unavailable,
}
