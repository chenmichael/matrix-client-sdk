﻿namespace MatrixClient.Schema.Enums;

/// <summary>
/// Different designations for room joining.
/// </summary>
[JsonConverter(typeof(StringEnumConverter))]
public enum JoinRule {
    /// <summary>
    /// Anyone can join the room without any prior action.
    /// </summary>
    [EnumMember(Value = "public")] Public,
    /// <summary>
    /// A user must first receive an invite from someone already in the room in order to join.
    /// </summary>
    [EnumMember(Value = "invite")] Invite,
    /// <summary>
    /// A user can request an invite to the room.They can be allowed(invited) or denied(kicked/banned) access.Otherwise, users need to be invited in. Only available in rooms which support knocking.
    /// </summary>
    [EnumMember(Value = "knock")] Knock,
    /// <summary>
    /// Anyone able to satisfy at least one of the allow conditions is able to join the room without prior action. Otherwise, an invite is required.Only available in rooms which support the join rule.
    /// </summary>
    [EnumMember(Value = "restricted")] Restricted,
    /// <summary>
    /// A user can request an invite using the same functions offered by the knock join rule, or can attempt to join having satisfied an allow condition per the restricted join rule.Only available in rooms which support the join rule.
    /// </summary>
    [EnumMember(Value = "knock_restricted")] Knock_restricted,
    /// <summary>
    /// Reserved without implementation.No significant meaning.
    /// </summary>
    [EnumMember(Value = "private")] Private,
}
