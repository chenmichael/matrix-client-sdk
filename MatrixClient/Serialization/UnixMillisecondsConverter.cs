﻿using System.Diagnostics.CodeAnalysis;

namespace MatrixClient.Serialization;

public class UnixMillisecondsConverter : JsonConverter<DateTime> {
    public override DateTime ReadJson(JsonReader reader, Type objectType, [AllowNull] DateTime existingValue, bool hasExistingValue, JsonSerializer serializer) {
        if (reader.Value is long ms)
            return DateTimeOffset.FromUnixTimeMilliseconds(ms).DateTime;
        throw new NotImplementedException("Read DateTime other than long!");
    }
    public override void WriteJson(JsonWriter writer, [AllowNull] DateTime value, JsonSerializer serializer) => throw new NotImplementedException("Write datetime!");
}
