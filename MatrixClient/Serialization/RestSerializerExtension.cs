﻿using RestSharp;

namespace MatrixClient.Serialization;

public static class RestSerializerExtension {
    public static RestClient UseNewtonsoftJsonSerializer(this RestClient client, JsonSerializerSettings? settings = null)
        => client.UseOnlySerializer(() => new NewtonsoftJsonSerializer() { Settings = settings });
}
