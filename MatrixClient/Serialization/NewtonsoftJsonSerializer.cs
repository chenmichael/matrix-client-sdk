﻿using System.Collections.Immutable;
using RestSharp;
using RestSharp.Serializers;

namespace MatrixClient.Serialization;
public class NewtonsoftJsonSerializer : IRestSerializer, ISerializer, IDeserializer {
    public JsonSerializerSettings? Settings { get; init; }
    public Formatting Formatting { get; init; } = Formatting.None;
    public ISerializer Serializer => this;
    public IDeserializer Deserializer => this;
    private const string DefaultContentType = "application/json";
    private static readonly string[] ContentTypesArray = new[] { DefaultContentType };
    private static readonly ImmutableHashSet<string> ContentTypes = ContentTypesArray.ToImmutableHashSet();
    public string[] AcceptedContentTypes { get; } = ContentTypesArray;
    public SupportsContentType SupportsContentType => ContentTypes.Contains;
    public DataFormat DataFormat => DataFormat.Json;
    public string ContentType { get; set; } = DefaultContentType;
    public string Serialize(Parameter parameter) {
        ArgumentNullException.ThrowIfNull(parameter.Value);
        return Serialize(parameter.Value);
    }
    public string Serialize(object obj) => JsonConvert.SerializeObject(obj, Formatting, Settings);
    public T Deserialize<T>(RestResponse response)
        => JsonConvert.DeserializeObject<T>(response.Content
            ?? throw new ArgumentNullException("Response content was empty!", nameof(response.Content)), Settings)
        ?? throw new ArgumentNullException("Deserialized object was null!");
}