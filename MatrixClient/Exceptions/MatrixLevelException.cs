﻿namespace MatrixClient.Exceptions;

[Serializable]
public class MatrixLevelException : RequestException {
    public MatrixLevelException(string message, HttpRequestMessage request, string errorCode, Exception? innerException = null) : base($"{errorCode}: {message}", request, innerException) => ErrorCode = errorCode;
    public string ErrorCode { get; }
}