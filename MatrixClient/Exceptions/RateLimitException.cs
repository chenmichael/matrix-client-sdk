﻿
namespace MatrixClient.Exceptions;
[Serializable]
public class RateLimitException : MatrixLevelException {
    public RateLimitException(string message, HttpRequestMessage request, string errorCode, int retryMs, Exception? innerException = null) : base(message, request, errorCode, innerException) => RetryMs = retryMs;
    public int RetryMs { get; }
}