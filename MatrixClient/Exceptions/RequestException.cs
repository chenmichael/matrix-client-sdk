﻿namespace MatrixClient.Exceptions;

[Serializable]
public abstract class RequestException : ApiException {
    public RequestException(string message, HttpRequestMessage request, Exception? innerException = null) : base(message, innerException) => Request = request;
    public HttpRequestMessage Request { get; }
}
