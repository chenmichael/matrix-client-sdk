﻿namespace MatrixClient.Exceptions;

[Serializable]
public class BadRequestException : RequestException {
    public BadRequestException(string message, HttpRequestMessage request, Exception? innerException = null) : base(message, request, innerException) { }
}
