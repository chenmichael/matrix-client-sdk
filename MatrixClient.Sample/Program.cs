﻿// See https://aka.ms/new-console-template for more information
using System.Text;
using MatrixClient;
using MatrixClient.Schema.Events;
using MatrixClient.Schema.Events.Room;
using MatrixClient.Schema.Messages;
using Newtonsoft.Json;

var client = new Client("matrix.cnml.de");
client.On<PresenceEvent>(e => Console.WriteLine($"{e} is now {e.Content.Presence} (last active {TimeSpan.FromMilliseconds(e.Content.LastActiveAgo ?? 0)})"));
client.On<RoomMemberEvent>(e => Console.WriteLine($"\tMembership changed {e.Content.DisplayName}: {e.Content.Membership}{(e.Content.IsDirect ? " (direct)" : "")}"));
client.On<RoomMessageEvent>(e => {
    Console.WriteLine($"\tMessage received from {e.Sender}:");
    switch (e.Content) {
    case TextMessage text:
        Console.WriteLine($"\t\t{text.Body}");
        break;
    case var cnt: Console.WriteLine($"Unhandled message content of type {cnt.GetType().Name}!"); break;
    }
});
client.On<DirectEvent>(e => {
    foreach (var (user, channels) in e.Content)
        Console.WriteLine($"Direct channels with {user}: {string.Join(", ", channels)}");
});

Console.WriteLine("Supported logins:");
Console.WriteLine(string.Join(", ", await client.GetSupportedLoginTypesAsync()));

await client.LoginAsync("mailbot", "mailbot");
Console.WriteLine($"Logged in as {client.UserId}");

client.StartListening(GetGetLastSyncBatch());
Console.ReadLine();

//await client.UpdateUserPresenceAsync(MatrixClient.Schema.Responses.UserPresence.Online, "Hello, there!");

//await foreach (var room in client.GetPublicRoomsAsync())
//    Console.WriteLine(room.Name ?? room.RoomId);

SaveLastSyncBatch(client.StopListening());


await client.LogoutAsync();
Console.WriteLine();

const string savefile = "syncState.dat";

static void SaveLastSyncBatch(string? syncState, Encoding? encoding = null) {
    encoding ??= Encoding.UTF8;
    if (syncState is null) File.Delete(savefile);
    else File.WriteAllBytes(savefile, encoding.GetBytes(syncState));
}
static string? GetGetLastSyncBatch(Encoding? encoding = null) {
    encoding ??= Encoding.UTF8;
    try {
        return encoding.GetString(File.ReadAllBytes(savefile));
    } catch (FileNotFoundException) { return null; }
}

